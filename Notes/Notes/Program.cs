﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Notes
{
    class Program
    {
        static void CreateNote() // создание заметки
        {
            Console.WriteLine("Введите имя нового файла");
            var name = Console.ReadLine();
            File.Create($"X:\\C# notes\\{name}.txt");
            Console.Clear();
            Console.ReadKey();
            Menu();
        }

        static void AddTextToNote() // добавление текста в заметку
        {
            Console.WriteLine("Введите название заметки для создания/редактирования и текст для записи в заметку.\nКаждая новая запись в существующей заметке создает новую строку");
            var name = Console.ReadLine();
            string text = Console.ReadLine();
            File.AppendAllText($"X:\\C# notes\\{name}.txt", $"{text}\n");
            Console.WriteLine("Изменения сохранены");
            Console.ReadKey();
            Console.Clear();
            Menu();
        }
        static void OpenNote()  // открыть для чтения
        {
            Console.WriteLine("Введите название заметки для чтения");
            var name = Console.ReadLine();
            if (Directory.EnumerateFiles(@"X:\\C# notes", $"*{name}*", SearchOption.AllDirectories).Any())
            {
                FileStream file = new FileStream($"X:\\C# notes\\{name}.txt", FileMode.Open);
                StreamReader reader = new StreamReader(file);
                Console.WriteLine(reader.ReadToEnd());
                reader.Close();
            }
            else
            {
                Console.WriteLine("Файл не найден");
            }
            Console.ReadKey();
            Console.Clear();
            Menu();
        }

        static void DeleteNote() // удаление заметки
        {
            Console.WriteLine("Введите имя заметки для удаления");
            var name = Console.ReadLine();
            if (Directory.EnumerateFiles(@"X:\\C# notes", $"*{name}*", SearchOption.AllDirectories).Any())
            {
                File.Delete($"X:\\C# notes\\{name}.txt");
                Console.WriteLine($"Заметка {name} удалена.");
            } else
            {
                Console.WriteLine("Заметка не найдена");
            }
            Console.ReadKey();
            Console.Clear();
            Menu();
        }

        static void FileList() //список файлов  в папке
        {
            string[] allfiles = Directory.GetFiles("X:\\C# notes");
            if (Directory.EnumerateFiles(@"X:\\C# notes", "*.*", SearchOption.AllDirectories).Any())
            {
                foreach (string filename in allfiles)
                {
                    Console.WriteLine(filename);
                }
            }
            else
            {
                Console.WriteLine("Папка пуста");
            }
            Console.ReadKey();
            Console.Clear();
            Menu();
        }
        static void Exit()
        {
            Console.WriteLine("Для выхода нажмите Enter дважды");
            Console.ReadKey();
        }

        static void Menu()
        {
            Console.WriteLine("Меню. Для выбора пункта меню, наберите номер пункта с клавиатуры");
            Console.WriteLine("1. Список заметок");
            Console.WriteLine("2. Открыть заметку");
            Console.WriteLine("3. Создать/открыть заметку и записать в неё текст");
            Console.WriteLine("4. Удалить заметку");
            Console.WriteLine("5. Выход");

            int userAnswer = Convert.ToInt32(Console.ReadLine());
            if (userAnswer == 1)
            {
                FileList();
            }
            else if (userAnswer == 2)
            {
                OpenNote();
            } else if (userAnswer == 3)
            {
                AddTextToNote();
            } else if (userAnswer == 4)
            {
                DeleteNote();
            } else if (userAnswer == 5)
            {
                Exit();
            } 
            else
            {
                Console.WriteLine("Вы ввели недопустимое значение");
                Menu();
            }
        }
        static void Main(string[] args)
        {
            try
            {
                Menu();
            } catch(Exception ex)
            {
                Console.WriteLine("В приложении возникла критическая ошибка, перезапустите приложение");
            }

            Console.ReadKey();
        }

    }
}
